/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mandelBrot.cpp

* Purpose : Generate the Mandelbrot set using MPI

* Creation Creation Date : 18-11-2017

* Last Modified : Sun 19 Nov 2017 08:17:37 PM EST

* Created By :  Albert R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#include <iostream>
#include <mpi.h>

using namespace std;

/**
 * @Description Struct that holds the data for a generic Mandelbrot point
 *
 * @param Re Type: Double, holds the value of the real component of the point
 * @param Im Type: Double, holds the value of the imaginary component of the point
 * @param iter Type: Int, holds  the number of iterations until a value of 2 is reached or maxIter
 * @param member Type: Int, holds the value 1 if it is a member of the set and 0 if not
 */
struct mandPt {
	double Re;
	double Im;
	int iter;
	int member;
	mandPt():Re(0.0),
			 Im(0.0),
			 iter(0),
			 member(0){}
	mandPt& operator=(mandPt rhs){
			if(this!=&rhs){
				this->Re=rhs.Re;
				this->Im=rhs.Im;
				this->iter=rhs.iter;
				this->member=rhs.member;
			}
			return *this;
		}

};




/**
 * @Description Overloading the (*) opertator for mandPt.  Calculation
 * of Real and imaginary parts uses the formula (a+bi)(c+di)=(ac-bd)+(ad+bc)i
 * In addition the values of .iter and .member from lhs are copied to the temp 
 * and returned with it.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt, the right hand side of the op
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator*(mandPt lhs, mandPt rhs){
	//mandPt *temp=new mandPt;
	mandPt temp;
	temp.Re=(lhs.Re*rhs.Re)-(lhs.Im*rhs.Im);
	temp.Im=(lhs.Re*rhs.Im)+(lhs.Im*rhs.Re);
	temp.iter=lhs.iter;
	temp.member=lhs.member;
	return temp;
}


/**
 * @Description Overloading the (+) operator for type mandPt.  In addition to
 * adding the real and imaginary parts the value of the iter and member variables
 * of the left hand side are copied.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt, the right hand side of the op
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator+(mandPt lhs, mandPt &rhs){
	//mandPt *temp=new mandPt;
	mandPt temp;
	temp.Re=lhs.Re+rhs.Re;
	temp.Im=lhs.Im+rhs.Im;
	temp.iter=lhs.iter;
	temp.member=lhs.member;

	return temp;
}


/**
 * @Description Overloading the (+) operator for type mandPt, with the rhs a pointer
 * to a mandPt.  In addition to adding the real and imaginary parts the value of the 
 * iter and member variables of the left hand side are copied.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt*, the right hand side of the op 
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator+(mandPt lhs, mandPt *rhs){
//	mandPt *temp=new mandPt;
	mandPt temp;
	temp.Re=lhs.Re+rhs->Re;
	temp.Im=lhs.Im+rhs->Im;
	temp.iter=lhs.iter;
	temp.member=lhs.member;

	return temp;
}


/**
 * @Description Checks if the modulus of the complex number indicates that the point
 * will escape to infinity.
 *
 * @param z Type: mandPt, the point being checked
 *
 * @return Type: int, 0 if the point escapes, 1 if it hasn't yet
 */
int modulusCheckerMandPt(const mandPt &z){
	if((((z.Re*z.Re)+(z.Im*z.Im)))>=4){return 0;}
	else{return 1;}
}


/**
 * @Description Checks to see if the point is an elemend of the Mandelbrot set.  This is done
 * by checking the modulos of z after each iteration.  If the modulus of z remains below 2 
 * and the loop gets to the maxIter then the point is in the set and .member is set to 1
 *
 * @param c Type: mandPT, the seed point
 * @param maxIter Type: int, holds the max number of iterations for the loop
 *
 * @return Type: int, return value other than 0 indicates and error
 */
int checkPoint(mandPt *c, const int maxIter){ 
	mandPt z, znext;
	for(int i=0; (i<maxIter)&&modulusCheckerMandPt(z); i++){
		znext=(z*z)+c;
		c->iter+=1;
		z=znext;
	}
	if(modulusCheckerMandPt(z)){c->member=1;}
	else{c->member=0;}
}


int main(int argc, char **argv){


	int rank, size, ierr, maxIter, numPtsRe, numPtsIm, numPtsTot, numPtsLocal;
	double stepRe, stepIm, maxRe, minRe, maxIm, minIm;

	long int setUpPtAddress, ReAddress, ImAddress, iterAddress, memberAddress;
	long int ReOffset, ImOffset, iterOffset, memberOffset, fileOffset;

	//assigns default values for the mesh if not provided at the command prompt
	if(argc==1){
		maxIter=10;
		stepRe=0.5;
		stepIm=0.5;
		maxRe=2.0;
		minRe=-2.0;
		maxIm=2.0;
		minIm=-2.0;
	}

	mandPt setUpPt;

	MPI_Status status;
	MPI_File output;

	ierr=MPI_Init(&argc, &argv);
	
	MPI_Datatype mandPtType;
	
	MPI_Get_address(&setUpPt, &setUpPtAddress);
	MPI_Get_address(&setUpPt.Re, &ReAddress);
	MPI_Get_address(&setUpPt.Im, &ImAddress);
	MPI_Get_address(&setUpPt.iter, &iterAddress);
	MPI_Get_address(&setUpPt.member, &memberAddress);

	ReOffset=ReAddress-setUpPtAddress;
	ImOffset=ImAddress-setUpPtAddress;
	iterOffset=iterAddress-setUpPtAddress;
	memberOffset=memberAddress-setUpPtAddress;


	//array holding the list of MPI datatypes in the order they occur in the struct. 
	MPI_Datatype type[4]={MPI_DOUBLE, MPI_DOUBLE, MPI_INT, MPI_INT};
    
	//Array holding address for offset purposes
	MPI_Aint displ[4]={ReOffset, ImOffset, iterOffset, memberOffset};

	//array holding the lengths of the datatypes, also in order the occur.
	//note that sizeof(datatype) is used to make the code more platform independent.
	int blockLen[4]={sizeof(double), sizeof(double), sizeof(int), sizeof(int)};


	//Creates the new MPI_Datatype
	ierr=MPI_Type_create_struct(4, blockLen, displ, type, &mandPtType);

	//Required before the type can be used with a communicator
	ierr=MPI_Type_commit(&mandPtType);


	ierr=MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	ierr=MPI_Comm_size(MPI_COMM_WORLD, &size);
	if(rank==0){
		numPtsRe=(int)((maxRe-minRe)/stepRe);
		numPtsIm=(int)((maxIm-minIm)/stepIm);
		numPtsTot=numPtsRe*numPtsIm;
		ierr=MPI_Bcast(&numPtsTot, 1, MPI_INT, 0, MPI_COMM_WORLD); 
	}

	//responsible for calculating which if any processes have to check and extra point
	if(rank<(numPtsTot%size)){numPtsLocal=(int)((numPtsTot/size)+1);}
	else{numPtsLocal=(int)(numPtsTot/size);}

	//creates the array to hold the points dynamically, so stored on the heap
	mandPt *pointsLocal = new mandPt[numPtsLocal];
	int temp=0;

	//Loops through the points like a matrix a row at at time
	for(double i=minIm+(rank*stepIm); i<=maxIm;i+=size*stepIm){
		for(double j=minRe+(rank*stepRe); j<=maxRe; j+=size*stepRe ){
			
			cout<<"\nbegin inner loop rank " <<rank<<"\n";
			pointsLocal[temp].Re=j;
			pointsLocal[temp].Im=i;
			ierr=checkPoint(&pointsLocal[temp], maxIter);
			temp++;
		}
	}
	//Calculate the offset/starting point in the file for each process
	fileOffset=rank*sizeof(mandPt)*numPtsLocal;

	cout << "\nafter loop rank" <<rank<<"\n";
	//write the output of checking the points to the file: results.txt
	ierr=MPI_File_open(MPI_COMM_WORLD, "results.txt", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &output);
	ierr=MPI_File_write_at(output, fileOffset, pointsLocal, numPtsLocal, mandPtType, &status);
	MPI_File_close(&output);
//	delete[] pointsLocal;

	MPI_Finalize();

	return 0;

}
