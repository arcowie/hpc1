CC = mpic++
CFLAGS = -Wall -pedantic -O3
LDFLAGS =
DIR = ~/bin/
PROG = mandelBrotMPI
OBJS = mandelBrot.cpp
 
all: $(PROG)
 
$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) -o $(DIR)$@ $< 
 
clean:
	rm -rf $(OBJS)
 
.PHONY: all clean

